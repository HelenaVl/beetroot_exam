$(document).ready(function() {
    $('a[href^="#"]').click(function() {
        elementClick = $(this).attr("href");
        destination = $(elementClick).offset().top; {
            $('html,body').animate({ scrollTop: destination }, 1500);
        }
        return false;
    });

//
    $(document).ready(function(){
        $('.team_slider').slick({
            dots: true,
            infinite: true,
            speed: 500,
            cssEase: 'linear'
        });
      });
});

// КАРТА
function initMap() {
    var uluru = {lat: 47.8600155, lng: 35.0257079};
    var zoomMap = 10
    var map = new google.maps.Map(document.querySelector('#map'), {
      zoom: zoomMap,
      center: uluru
    });
    var marker = new google.maps.Marker({
      title: 'Bouncy',
      position: uluru,
      draggable:true,
      animation: google.maps.Animation.DROP,
      map: map,
    });
  }